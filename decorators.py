import logging
import sys
import os, hashlib
from datetime import datetime

def get_logger(file, name=__file__, encoding='utf-8'):  

    if not file :
        file='log.txt'

    log = logging.getLogger(name)
    log.setLevel(logging.INFO)            
    formatter = logging.Formatter('[%(asctime)s] %(filename)s:%(lineno)d %(levelname)-10s %(message)s')

    fh = logging.FileHandler(file, encoding=encoding)
    fh.setFormatter(formatter)
    log.addHandler(fh)

    sh = logging.StreamHandler(stream=sys.stdout)
    sh.setFormatter(formatter)
    log.addHandler(sh)
    return log

def make_try(path=None):
   
    def logger(function):
        
        def wrapper(*args, **kwargs):
            log = get_logger(file=path)
            try:
                result = function(*args, **kwargs)
                log.info(f"Function: {function.__name__}() arguments: {args} result: {result}")
                
            except Exception:
                err = f"Exception in {function.__name__}() arguments: {args} "
                log.exception(err)
                pass

            return result

        return wrapper

    return logger


path = '111'

@make_try()
def division(a, b):
    return int(a / b)


@make_try()
def summator(x, y):
   return x + y


if __name__ == '__main__':
    res = division(20, 1)
    res = division(10, 5)

    three = summator(1, 2)
    five = summator(5, 0)
    result = summator(three, five)

    # print('result: ', result)
    # print('result type: ', type(result))

